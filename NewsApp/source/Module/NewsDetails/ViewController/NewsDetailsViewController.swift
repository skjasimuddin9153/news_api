//
//  NewsDetailsViewController.swift
//  NewsApp
//
//  Created by Techwens on 01/04/24.
//

import UIKit

class NewsDetailsViewController: UIViewController {


    
    @IBOutlet weak var llbTitle: UILabel!
    @IBOutlet weak var lbAuthor: UILabel!
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var lbNewsDescription: UILabel!
    var newsdata : Article?
    override func viewDidLoad() {
        super.viewDidLoad()
       
        setPage()
        
        let imageguesture =  UITapGestureRecognizer(target: self, action: #selector(onImageClicked))
        
        image.isUserInteractionEnabled = true
        image.addGestureRecognizer(imageguesture)
        
    }
    
   
    
    @objc private func onImageClicked(){
        
        guard let vc = ZoomingViewController.loadFromXIB() else {return}
        vc.image = image.image
        self.navigationController?.pushViewController(vc, animated:true)
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    func setPage() {
        lbAuthor.layer.cornerRadius = 10
        // Unwrap newsdata
     
        
        // Set title and author labels
        if let title =   newsdata?.title , let author = newsdata?.author {
            llbTitle.text = "\(title)"
            lbAuthor.text = "Author:- \(author)"
        }
//        lbAuthor.text = newsdata.author
        
        // Set news description label if available
        if let description = newsdata?.description {
            lbNewsDescription.text = description
        } else {
            // Set some default text or hide the label if description is nil
            lbNewsDescription.text = "No description available"
        }
    }

    
    
    
    @IBAction func onLinkClick(_ sender: Any) {
        guard let vc = WebViewViewController.loadFromXIB() else {return}
        vc.urlString = newsdata?.url ?? ""
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension NewsDetailsViewController {
    class func loadFromXIB() -> NewsDetailsViewController? {
        let storyboard = UIStoryboard(name: "NewsDetails", bundle: nil)
        guard let viewController = storyboard.instantiateViewController(withIdentifier: "NewsDetailsViewController") as? NewsDetailsViewController else {
            return nil
        }
      
        return viewController
    }
}
