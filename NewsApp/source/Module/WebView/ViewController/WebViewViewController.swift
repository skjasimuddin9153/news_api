import UIKit
import WebKit

class WebViewViewController: UIViewController {

    @IBOutlet weak var webView: WKWebView!
    var activityIndicator: UIActivityIndicatorView!
    
    var urlString: String = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Set self as the delegate of WKWebView
        webView.navigationDelegate = self
      
              
              // Create and configure the activity indicator
              activityIndicator = UIActivityIndicatorView(style: .large)
              activityIndicator.center = view.center
              activityIndicator.hidesWhenStopped = true
              view.addSubview(activityIndicator)
        
        loadWeb()
       
    }
    
    private func loadWeb() {
        guard let url = URL(string: urlString) else {
            // Handle invalid URL string
            print("Invalid URL")
            return
        }
        
        let urlRequest = URLRequest(url: url)
        webView.load(urlRequest)
    }
}

extension WebViewViewController {
    class func loadFromXIB() -> WebViewViewController? {
        let storyboard = UIStoryboard(name: "WebView", bundle: nil)
        guard let viewController = storyboard.instantiateViewController(withIdentifier: "WebViewViewController") as? WebViewViewController else {
            return nil
        }
      
        return viewController
    }
}
extension WebViewViewController: WKNavigationDelegate {
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        // Show activity indicator when web view begins loading
        activityIndicator.startAnimating()
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        // Hide activity indicator when web view finishes loading
        activityIndicator.stopAnimating()
    }
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        // Hide activity indicator if loading fails
        activityIndicator.stopAnimating()
    }
}
