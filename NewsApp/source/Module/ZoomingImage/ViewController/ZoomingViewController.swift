//
//  ZoomingViewController.swift
//  NewsApp
//
//  Created by Techwens on 01/04/24.
//

import UIKit

class ZoomingViewController: UIViewController {

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var imageView: UIImageView!
    
    var image : UIImage?
    override func viewDidLoad() {
        super.viewDidLoad()
        scrollView.delegate = self
        
        imageView.image = image 
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension ZoomingViewController : UIScrollViewDelegate {
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return imageView
    }

}

extension ZoomingViewController {
    class func loadFromXIB() -> ZoomingViewController? {
        let storyboard = UIStoryboard(name: "ZoomImage", bundle: nil)
        guard let viewController = storyboard.instantiateViewController(withIdentifier: "ZoomingViewController") as? ZoomingViewController else {
            return nil
        }
      
        return viewController
    }
}
