import Foundation




final class NewsListingViewModel {
  
    

    enum Event {
        case loading
        case stopLoading
        case dataLoaded
        case error(Error)
        case data(NewsResponse)
    }

    var eventHandler: ((Event) -> Void)?

    // Your existing login and other functions here...

    func getmapData() {
        self.eventHandler?(.loading) // Notify the loading state
        
      
        APIManager.shared.request(
            method: .get,
            endpoint:MapEndPoint.mapList(apikey: Constant.shared.apiKey),
            parameters: [:]
        ) { result in
            switch result {
            case .success(let data):
                do {
                    let json: NewsResponse = try JSONDecoder().decode(NewsResponse.self, from: data)
                    print("Success: \(json)")
                  
                    self.eventHandler?(.data(json)) // Notify success and pass data
                    NewsdataManager.shared.newsListdata = json
                    self.eventHandler?(.dataLoaded) // Notify data loaded
                } catch {
                    print("Error parsing JSON: \(error)")
                    self.eventHandler?(.error(error))//Notify error
                }
            case .failure(let error):
                self.eventHandler?(.error(error))
            }
        }
    }
}

