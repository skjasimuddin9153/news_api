//
//  NewsListModel.swift
//  NewsApp
//
//  Created by Techwens on 01/04/24.
//

import Foundation


// Struct to represent the source of the article
struct Source: Codable {
    let id: String?
    let name: String
}

// Struct to represent an article
struct Article: Codable {
    let source: Source
    let author: String?
    let title: String
    let description: String?
    let url: String
    let urlToImage: String?
    let publishedAt: String
    let content: String?
}

// Struct to represent the API response
struct NewsResponse: Codable {
    let status: String
    let totalResults: Int
    let articles: [Article]
}
