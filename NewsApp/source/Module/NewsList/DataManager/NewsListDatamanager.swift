import Foundation

protocol NewsListDatamanagerProtocolos: AnyObject {
    func updatedData(data: NewsResponse)
}

class NewsdataManager {
    static let shared = NewsdataManager()
    
    var newsListdata: NewsResponse? {
        didSet {
            // Check if newsListdata is not nil and if delegate is set
            if let newData = newsListdata {
                delegate?.updatedData(data: newData)
            }
        }
    }
    
    weak var delegate: NewsListDatamanagerProtocolos?
}

