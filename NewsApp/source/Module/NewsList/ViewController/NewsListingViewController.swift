//
//  newsListingViewController.swift
//  NewsApp
//
//  Created by Techwens on 01/04/24.
//

import UIKit

class NewsListingViewController: UIViewController {
    
    lazy var viewModel = NewsListingViewModel()
    
    var data : NewsResponse?

    @IBOutlet weak var tvNews: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
       

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        viewModel.getmapData()
        observeLoginViewModelEvents()
    }
    
    private func observeLoginViewModelEvents() {
        viewModel.eventHandler = { [weak self] event in
            guard let self = self else { return }
            
            switch event {
            case .loading:
                print("Loading...")
              
            case .stopLoading:
                print("Stop loading...")
           
            
            case .dataLoaded:
                print("Data loaded...")
               
            case .error(let error):
                print("Error: \(error)")
            case .data(let data):
                self.data = data
                print("dataLoaded", data)
                self.tvNews.reloadData()
            }
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

// MARK: - Load from storyboard with dependency
extension NewsListingViewController {
    class func loadFromXIB() -> NewsListingViewController? {
        let storyboard = UIStoryboard(name: "NewsList", bundle: nil)
        guard let viewController = storyboard.instantiateViewController(withIdentifier: "NewsListingViewController") as? NewsListingViewController else {
            return nil
        }
      
        return viewController
    }
}


extension NewsListingViewController : UITableViewDataSource , UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 210
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data?.articles.count ?? 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : NewsListTableViewCell = tvNews.dequeueReusableCell(withIdentifier: "cell") as! NewsListTableViewCell
        
        if let itemdata : Article = data?.articles[indexPath.row] {
            
            cell.updateUi(item: itemdata)
        }
        cell.selectionStyle = .none
        
        return cell 
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        guard let vc = NewsDetailsViewController.loadFromXIB() else {return}
        vc.newsdata =  data?.articles[indexPath.row]
        self.navigationController?.pushViewController(vc, animated: true)
        
        
    }
    
    
}
