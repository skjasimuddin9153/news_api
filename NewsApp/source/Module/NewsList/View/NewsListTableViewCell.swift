//
//  NewsListTableViewCell.swift
//  NewsApp
//
//  Created by Techwens on 01/04/24.
//

import UIKit

class NewsListTableViewCell: UITableViewCell {

    @IBOutlet weak var parrentView: UIView!
    @IBOutlet weak var lbTitle: UILabel!
    
    @IBOutlet weak var lbsubtitle: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        parrentView.layer.cornerRadius = 10
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func updateUi(item:Article) {
        let formatedData = Dateformate.shared.formatDate(item.publishedAt)
        lbTitle.text = "\(item.title)"
        
        if let author = item.author , let date = formatedData {
            lbsubtitle.text = "Auther :-  \(author) on \(date)"
        }
        
       
    }
    
}
