import UIKit

class SplashViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 3.0) { [weak self] in
            guard let self = self else { return }
            if self.isFirstTimeLogin() {
                guard let vc = NewsListingViewController.loadFromXIB() else { return }
               
                self.navigationController?.replaceTopViewController(with: vc, animated: true)
                
            }
        }
    }
    
    func isFirstTimeLogin() -> Bool {
        // Mark:-  If user login then return true
        return true // Replace this with your login logic
    }
}
