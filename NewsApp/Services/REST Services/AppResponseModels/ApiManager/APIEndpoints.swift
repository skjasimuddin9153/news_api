import Foundation
import Alamofire

public protocol APIEndpoint {

    var apiPath: String {get}
    var method: HTTPMethod {get}
}

extension APIEndpoint {

    var urlPath: String {

        return ServerConfig.baseURL + apiPath
    }
}

// MARK: - Example

enum MapEndPoint : APIEndpoint {
    case mapList(apikey:String)
   
//    ?sources=google-news&apiKey=API_K
    var apiPath: String {
        switch self {
        case .mapList(apikey:let apikey):
            return "?sources=google-news&apiKey=\(apikey)"
       
        
        }
    }
    var method: HTTPMethod {
        switch self {
        case .mapList:
            return .get
     
        }
    }
    
}


