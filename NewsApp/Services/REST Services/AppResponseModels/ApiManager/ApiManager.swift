import Alamofire
import Foundation

class APIManager {
    static let shared = APIManager()
    
    func request(
        method: HTTPMethod,
        endpoint: APIEndpoint,
        parameters: Parameters? = nil,
        headers: HTTPHeaders? = [
            HTTPHeader(name: "Content-Type", value: "application/json")
        ],
        completion: @escaping (Result<Data, Error>) -> Void
    ) {
     
        print("Api requesting....\(endpoint.urlPath)")
        print("method --> \(method)")
        print("parameter --> \(parameters)")
        print("header --> \(headers)")
        
        var urlRequest = URLRequest(url: URL(string: endpoint.urlPath)!)
        urlRequest.httpMethod = method.rawValue
        
        // Set headers individually
        headers?.forEach { header in
            urlRequest.setValue(header.value, forHTTPHeaderField: header.name)
        }
        
        if method != .get {
            // If it's not a GET request, encode parameters in the request body
            if let parameters = parameters {
                do {
                    urlRequest.httpBody = try JSONSerialization.data(withJSONObject: parameters)
                } catch {
                    completion(.failure(error))
                    return
                }
            }
        }
        
        AF.request(urlRequest)
            .validate()
            .responseData { response in
                switch response.result {
                case .success(let data):
                    completion(.success(data))
                    print("success api calling")
                case .failure(let error):
                    completion(.failure(error))
                    print("error api calling \(error)")
                }
            }
    }
}
