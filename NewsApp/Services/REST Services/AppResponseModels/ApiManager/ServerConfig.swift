import Foundation
import Alamofire
import UIKit


enum APIMode {
    case localDevelopment
    case production
    case staging
    case development
}

struct ServerConfig {

    // MARK: - Static stored properties
    static var baseURL = "https://newsapi.org/v2/top-headlines"
    static var dataUrl = "https://newsapi.org/v2/top-headlines"

    // MARK: Setting mode changes the stored URLs
    static var mode: APIMode = .production {
        didSet {
            switch mode {

            case .localDevelopment:
                baseURL = ""
                dataUrl = ""
            case .production:
                baseURL = ""
                dataUrl = ""
            case .staging:
                baseURL = "https://newsapi.org/v2/top-headlines"
                dataUrl = "https://newsapi.org/v2/top-headlines"
            case .development:
                baseURL = ""
                dataUrl = ""
            }
        }
    }

    // MARK: - Initializer
    init(withMode mode: APIMode) {
        ServerConfig.mode = mode
    }
}

struct APIHeader {

    internal enum HeaderKeys: String {

        case contentType = "Content-Type"
        case accept = "Accept"
        case uuid = "x-auth-deviceid"
        case deviceType = "device_type"
        case pushToken = "x-auth-notificationKey"
        case authToken = "Authorization"
        case appVersion = "appVersion"
        case localization = "x-auth-lang"
    }

    static let shared = APIHeader()

    var uuidString: String {
        return UIDevice.current.identifierForVendor?.uuidString ?? ""
    }

   

   

    var headers: HTTPHeaders {

        var headerDict = [
            HeaderKeys.contentType.rawValue: "application/json",
            HeaderKeys.accept.rawValue: "application/json",
            HeaderKeys.uuid.rawValue: uuidString,
            HeaderKeys.deviceType.rawValue: "2",
        
        ]

      

      
      

        return HTTPHeaders(headerDict)
    }

    var multipartHeaders: HTTPHeaders {

        var headerDict = [
            "Content-Type": "multipart/form-data; boundary=\(UUID().uuidString)",
            HeaderKeys.accept.rawValue: "application/json",
            HeaderKeys.uuid.rawValue: uuidString,
            HeaderKeys.deviceType.rawValue: "ios"
        ]

      

      

       
        return HTTPHeaders(headerDict)
    }
}



